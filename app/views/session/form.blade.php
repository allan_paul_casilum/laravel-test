{{ Form::open(array('route' => 'session.store','class'=>'form-signin')) }}
  @if (Session::has('login_errors'))
	<div class="alert alert-danger">Username or password incorrect.</div>
  @endif
  <h2 class="form-signin-heading">Please sign in</h2>
	<div class="form-group">
		{{ Form::text('email','',array('class'=>'form-control','placeholder'=>'Email address','required','autofocus')) }}
	</div>
	<div class="form-group">
		{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password','required')) }}
	</div>
	<p></p>
 <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
 <p><a href="{{url('password')}}">Forgot Passowrd?</a></p>
{{ Form::close() }}
{{ Form::open(array('route' => 'register.store','class'=>'form-signin')) }}
  <h2 class="form-signin-heading">Or Sign Up</h2>

  @include('user.register.input')

 <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>

{{ Form::close() }}

