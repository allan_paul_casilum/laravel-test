@extends('master')

@section('head-css')
	@parent
@stop

@section('head-js')

@stop

@section('maincontent')
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
	@include('session.form')
@stop

@section('footer-js')
@parent

@stop

