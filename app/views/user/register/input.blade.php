<div class="form-group">
{{ Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email address','required','autofocus')) }}
</div>
<div class="form-group">
{{ Form::text('name',null,array('class'=>'form-control','placeholder'=>'Your Name','required','autofocus')) }}
</div>
@if( Auth::check() )
	<div class="form-group">
	{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
	</div>
	<div class="form-group">
	{{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Confirm Password')) }}
	</div>
@else
	<div class="form-group">
	{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password','required','autofocus')) }}
	</div>
	<div class="form-group">
	{{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Confirm Password','required','autofocus')) }}
	</div>
@endif

