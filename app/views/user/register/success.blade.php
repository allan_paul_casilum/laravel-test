@extends('master')

@section('head-css')
	@parent
	<link href="{{URL::asset('public/css/dashboard.css');}}" rel="stylesheet" media="screen">
@stop

@section('head-js')

@stop

@section('notificationsystem')
@stop

@section('maincontent')
		{{--@if (Session::has('message'))--}}
			<div class="jumbotron">
			  <div class="container">
				<h1>{{ Session::get('message') }}</h1>
				<p>Please Check Email</p>
				<p><a class="btn btn-primary btn-lg" href="{{url('login')}}" role="button">Or Click to Login</a></p>
			  </div>
			</div>
		{{--@endif--}}
@stop

@section('footer-js')
@parent
@stop




