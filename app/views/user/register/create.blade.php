@extends('master')

@section('head-css')
	@parent
	<link href="{{URL::asset('public/css/dashboard.css');}}" rel="stylesheet" media="screen">
@stop

@section('head-js')

@stop

@section('notificationsystem')
	{{ HTML::ul($errors->all(),array('class' => 'list-group list-unstyled error')) }}

	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
@stop

@section('maincontent')
	<div class="main">
		{{ Form::open(array('route' => 'register.store','role'=>'form')) }}
			@include('user.register.input')
			<button class="btn btn-primary" type="submit">Sign Up</button>
		{{ Form::close() }}
		<p><a href="{{url('login')}}">Already Registered? Please click to login</a></p>
	</div>
@stop

@section('footer-js')
@parent
@stop







