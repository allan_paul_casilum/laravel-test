<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Laravel Test</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::asset('public/bootstrap-3.1.1/css/bootstrap.min.css');}}" rel="stylesheet" media="screen">
    <link href="{{URL::asset('public/bootstrap-3.1.1/css/bootstrap-theme.min.css');}}" rel="stylesheet" media="screen">
    <!-- Custom styles for this template -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{URL::asset('public/js/html5shiv.js');}}"></script>
      <script src="{{URL::asset('public/js/respond.min.js');}}"></script>
    <![endif]-->

    @section('head-css')
		<link href="{{URL::asset('public/css/login.css');}}" rel="stylesheet" media="screen">
	@show

	@section('head-js')

	@show

  </head>

  <body>

    <div class="container">
	  @section('notificationsystem')
	  @show
	  @section('maincontent')
      @show
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    @section('footer-js')
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="{{ URL::asset('public/js/jquery.min.js'); }}"></script>
		<script src="{{ URL::asset('public/bootstrap-3.1.1/js/bootstrap.min.js'); }}"></script>
		<script src="{{ URL::asset('public/bootstrap-3.1.1/js/holder.js'); }}"></script>
		<script src="{{ URL::asset('public/js/test.js'); }}"></script>
    @show
  </body>
</html>
