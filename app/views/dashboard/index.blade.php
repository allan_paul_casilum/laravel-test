@extends('master')

@section('head-css')
	@parent
	<link href="{{URL::asset('public/css/dashboard.css');}}" rel="stylesheet" media="screen">
@stop

@section('head-js')

@stop

@section('notificationsystem')
	{{ HTML::ul($errors->all(),array('class' => 'list-group error')) }}

	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
@stop

@section('maincontent')
	<div class="main dashboard-main">
		<h2>Welcome, {{Auth::user()->name}}</h2>
		{{ Form::model($user, array('route' => array('user.update', $user->id), 'role'=>'form', 'method' => 'PUT')) }}
			@include('user.register.input')
			<button class="btn btn-primary" type="submit">Update</button>
			<a href="{{url('logout')}}" class="btn btn-default" role="button">Logout</a>
		{{ Form::close() }}
	</div>
@stop

@section('footer-js')
@parent
@stop
