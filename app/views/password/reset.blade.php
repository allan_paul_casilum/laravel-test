@extends('master')

@section('head-css')
	@parent
	<link href="{{URL::asset('public/css/dashboard.css');}}" rel="stylesheet" media="screen">
@stop

@section('head-js')

@stop

@section('notificationsystem')
	@if (Session::has('error'))
	  <div class="alert alert-danger">{{ trans(Session::get('error')) }}</div>
	@elseif (Session::has('status'))
	  <div class="alert alert-info">An email with the password reset has been sent.</div>
	@endif
@stop

@section('maincontent')
	<div class="main">
		{{ Form::open(array('route' => array('password.update', $token),'class'=>'form-signin', 'method' => 'PUT')) }}
		  <div class="form-group">
			{{ Form::text('email','',array('class'=>'form-control','placeholder'=>'Email address','required','autofocus')) }}
		  </div>
		  <div class="form-group">
			{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password','required')) }}
		  </div>
		  <div class="form-group">
			{{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Confirm Password','required','autofocus')) }}
		  </div>
		 <p></p>
		 {{ Form::hidden('token', $token) }}
		 <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
		{{ Form::close() }}
	</div>
@stop

@section('footer-js')
@parent
@stop
