@extends('master')

@section('head-css')
	@parent
	<link href="{{URL::asset('public/css/dashboard.css');}}" rel="stylesheet" media="screen">
@stop

@section('head-js')

@stop

@section('notificationsystem')
	@if (Session::has('error'))
	  <div class="alert alert-danger">{{ trans(Session::get('error')) }}</div>
	@elseif (Session::has('status'))
	  <div class="alert alert-info">An email with the password reset has been sent.</div>
	@endif
@stop

@section('maincontent')
	<div class="main">
		{{ Form::open(array('route' => 'password.store', 'class'=>'form-signin')) }}
		  {{ Form::text('email','',array('class'=>'form-control','placeholder'=>'Email address','required','autofocus')) }}
		 <p></p>
		 <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
		{{ Form::close() }}
	</div>
@stop

@section('footer-js')
@parent
@stop





