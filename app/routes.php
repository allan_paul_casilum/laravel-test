<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('login', array(
  'uses' => 'SessionController@create',
  'as' => 'session.create'
));
Route::post('login', array(
  'uses' => 'SessionController@store',
  'as' => 'session.store'
));
Route::get('logout', array(
  'uses' => 'SessionController@destroy',
  'as' => 'session.destroy'
));

Route::resource('register', 'RegisterUserController');
Route::resource('user', 'UserController');

Route::get('success', 'RegisterUserController@Success');
Route::resource('password', 'RemindersController', array(
    'only' => array('index', 'store', 'show', 'update')
));;
Route::group(array('before' => 'auth'), function()
{
	Route::get('/', 'DashboardController@index');
});
/*
Route::get('/', function()
{
	return View::make('hello');
});

Route::get('test', function()
{
	$data['username'] = 'test';
	$data['password'] = 'password';
	$data['name'] = 'Allan Paul Casilum';
	$data['to'] = 'allan.paul.casilum@gmail.com';
	$data['subject'] = 'Welcome';

	Mail::send('emails.welcome', $data, function($message) use($data)
	{
		$message->to($data['to'], $data['name'])->subject($data['subject']);
	});
});
*/
