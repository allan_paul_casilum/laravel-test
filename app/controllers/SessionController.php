<?php

class SessionController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('session.login');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if( !Auth::check() ) {
			return View::make('session.login');
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$rules = array(
			'email'  => 'required',
			'password'	 => 'required|alphaNum|min:3'
		);

		$credentials = array(
			'email' 	=> Input::get('email'),
			'password' 	=> Input::get('password')
		);

		$validator = Validator::make($credentials,$rules);
		if($validator->passes()){
			if ( Auth::attempt($credentials) ){
			  return Redirect::intended('/');
			}
		}
		return Redirect::route('session.create')
				->withInput()
				->with('login_errors', true);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::to('login');
	}

}
