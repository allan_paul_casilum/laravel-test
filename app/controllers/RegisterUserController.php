<?php

class RegisterUserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if( !Auth::check() ){
			return View::make('user.register.create');
		}else{
			return Redirect::to('/');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.register.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::except('_token');

		$rules = array(
			'email' => 'required|email|unique:users,email',
			'name' => 'required',
			'password' => 'required|alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:6,12'
		);

		$validator = Validator::make($input, $rules);

		if ( $validator->passes() ) {

			$input = Input::except('_token','_method','password_confirmation');

			$password = $input['password'];
			$input['password'] = Hash::make($input['password']);

			$user = new User;
			$user->insertData( $input );

			//email for confirmation and account
			$data['username'] 	= $input['email'];
			$data['password'] 	= $password;
			$data['name'] 		= $input['name'];
			$data['to'] 		= $input['email'];
			$data['subject'] 	= 'Welcome';

			Mail::send('emails.welcome', $data, function($message) use($data)
			{
				$message->to($data['to'], $data['name'])->subject($data['subject']);
			});

			Session::flash('message', 'Successfully Register!');
			return Redirect::to('success');
		}else{
			return Redirect::to('register/create')->withErrors($validator)->withInput(Input::except('password'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	/**
	 * Display this afer successfull register
	 *
	 * @return Response
	 * */
	public function Success(){
		return View::make('user.register.success');
	}
}
