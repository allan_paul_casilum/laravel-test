<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('user.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::except('_token');

		$rules = array(
			'email' => 'required|email|unique:users,email,' . Auth::user()->id,
			'name' => 'required',
			'password' => 'alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'alpha_num|between:6,12'
		);

		$validator = Validator::make($input, $rules);

		if ( $validator->passes() ) {
			if( !$validator->messages()->has('password') && trim($input['password']) != ''  ){
				$input = Input::except('_token','_method','password_confirmation');
				$input['password'] = Hash::make($input['password']);
			}else{
				$input = Input::except('_token','_method','password','password_confirmation');
			}

			$input = Input::except('_token','_method','password_confirmation');

			$input['password'] = Hash::make($input['password']);

			$user = new User;
			$user->updateData( Auth::user()->id, $input );

			//email for confirmation and account

			Session::flash('message', 'Successfully Updated Profile!');
			return Redirect::to('/');
		}else{
			return Redirect::to('/')->withErrors($validator)->withInput(Input::except('password'));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
